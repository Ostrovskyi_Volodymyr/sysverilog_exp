#include <iostream>
#include <memory>
#include "Vmux.h"
#include <verilated.h>
#include <verilated_vcd_c.h>

vluint64_t main_time = 0;

int main(int argc, char **argv) {
	Verilated::commandArgs(argc, argv);
	Verilated::traceEverOn(true);
	
	std::unique_ptr<Vmux> mux(new Vmux);
	std::unique_ptr<VerilatedVcdC> tfp(new VerilatedVcdC);
    mux->trace(tfp.get(), 1);
    tfp->open("mux_simulation.vcd");

	mux->sel = 0;
	mux->din_0 = 0;
	mux->din_1 = 0;

	while (!Verilated::gotFinish()) {
		if (main_time < 1) {
			mux->sel = 0;
			mux->din_0 = 1;
		} else if (main_time < 2) {
			mux->sel = 1;
		} else if (main_time < 3) {
			mux->din_1 = 1;
		} else {
			Verilated::gotFinish(true);
		}
		mux->eval();
		tfp->dump(main_time);
		std::cout << uint32_t(mux->mux_out) << std::endl;
		main_time++;
	}
	mux->final();
	tfp->close();
}